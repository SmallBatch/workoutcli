import Ember from 'ember';
import weeks from 'workout/data/weeks';
import days from 'workout/data/days';
import workouts from 'workout/data/workouts';

export default Ember.Route.extend({
  beforeModel: function(){

    var promises = {
      milestones: this.get('store').find('milestone')
    };

    return Ember.RSVP.hash(promises);
  },
  model: function(params){

    var week_id = +params.week_id;

    var week = Ember.Object.create(weeks.findBy('id', week_id));
    var filteredDays =  days.filterBy('week', week_id);
    var map = filteredDays.map(this.dayCompleted, this);
    week.set('days',filteredDays);
    return week;
  },
  dayCompleted: function(day){
    var countWorkouts = workouts.filterBy('group', day.group).get('length');

    var milestones = this.get('store').all('milestone');

    var dayMilestones = milestones.filter(function(item){
      return (day.id === item.get('day_id') && item.get('workout_id') !== 0);
    }, day);

    day.isComplete = (dayMilestones.get('length') === countWorkouts);

    return day;
  }
});
