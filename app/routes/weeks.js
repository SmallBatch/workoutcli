import Ember from 'ember';
import weeks from 'workout/data/weeks';
import days from 'workout/data/days';

export default Ember.Route.extend({
  beforeModel: function(){
    var promises = {
      milestones: this.get('store').find('milestone')
    };

    return Ember.RSVP.hash(promises);
  },
  model: function() {
    return weeks.map(this.checkComplete, this);
  },
  checkComplete: function(week){
    var milestones = this.get('store').all('milestone');
    var completedDays = milestones.filter(function(item){
      return item.get('week_id') === week.id &&  item.get('workout_id') === 0 && item.get('day_id') !== 0;
    });
    var weekDays = days.filterBy('week', week.id);
    week.isComplete = weekDays.get('length') === completedDays.get('length');
    return week;
  }
});
