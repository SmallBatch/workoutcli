import Ember from 'ember';
import days from 'workout/data/days';
import exercises from 'workout/data/exercises';
import workouts from 'workout/data/workouts';

export default Ember.Route.extend({
  beforeModel: function(){

    var promises = {
      sets: this.get('store').find('set'),
      milestones: this.get('store').find('milestone')
    };

    return Ember.RSVP.hash(promises);
  },
  model: function(params){
    var key = params.day_id - 1;
    var day = Ember.Object.create(days[key]);
    var store = this.get('store');

    // this is coming from ember data (local storage)
    var sets = store.all('set');
    var milestones = store.all('milestone');

    var objWorkouts = workouts.filter(function(workout){

      if(workout.group === day.group) {

        workout.isComplete = milestones.findBy('workout_id', workout.id);

        if (typeof(workout.exercise)  === 'number') {
            workout.exercise = exercises.findBy('id', workout.exercise);
        }
        var setRecords = [];

        for(var i = 1; i <= workout.sets; i++){

          var rawItem = {set: i, day: day.id, workout: workout.id};
          var currentSet = sets.find(this.confirmCurrentSet, rawItem);
          var item = currentSet;

          if(Ember.isNone(currentSet)){
            rawItem.reps = workout.reps;
            rawItem.weight = '';
            item = store.createRecord('set', rawItem);
          }
          setRecords.push(item);
        }

        workout.setRecords = setRecords;
        return true;
      }

    }, this);

    day.set('workouts', objWorkouts);

    return day;
  },
  confirmCurrentSet: function(item){
    var set = item.get('set');
    var day = item.get('day');
    var workout = item.get('workout');

    if(set === this.set && day === this.day && workout === this.workout){
      return true;
    }
  },
  actions: {
    saveWorkout: function(workout) {

      var day_id = +this.paramsFor('day').day_id;
      var day = Ember.Object.create(days[day_id - 1]);
      var milestones = this.get('store').all('milestone');
      var expectedWorkouts = workouts.filterBy('group', day.group).get('length');

      var currentWorkoutMilestone = milestones.filterBy('workout_id', workout.id);

      if(currentWorkoutMilestone.get('length')===0){
        var newWorkoutMilestone = {workout_id: workout.id, day_id: day_id, week_id: day.week, completed: new Date()};
        currentWorkoutMilestone = this.get('store').createRecord('milestone', newWorkoutMilestone);
        currentWorkoutMilestone.save();
      }

      var dayMilestone = this.get('store').all('milestone').filter(function(item){
        return item.get('day_id') === this.id && item.get('workout_id') === 0;
      }, day);

      var dayWorkoutMilestones = this.get('store').all('milestone').filter(function(item){
        return item.get('day_id') === this.id && item.get('workout_id') !== 0;
      }, day);


      console.log('found the milestone for this day:', dayMilestone);

      console.log('number of workouts expected:', expectedWorkouts);
      console.log('number of workouts actually found:', dayWorkoutMilestones.get('length'));
      console.log(dayMilestone.get('length'));

      if( dayMilestone.get('length') === 0 && expectedWorkouts === dayWorkoutMilestones.get('length')){
        console.log('Making me a whole new day workout thingy');
        var newDayMilestone = {workout_id: 0, day_id: day_id, week_id: day.week, completed: new Date()};
        this.store.createRecord('milestone', newDayMilestone).save();
      }
    }
  },
  searchMilestones: function(){
    var milestones = this.get('store').all('milestone');
    //return milestones.filter()
  }
});
