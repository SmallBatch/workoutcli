import Ember from 'ember';
import exercises from 'workout/data/exercises';

export default Ember.Route.extend({
  model: function(params){
    return exercises.findBy('id', +params.exercise_id);
  },
  actions: {
    goBack: function(){
      history.back();
    }
  }
});
