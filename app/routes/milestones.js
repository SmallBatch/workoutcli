import Ember from 'ember';

export default Ember.Route.extend({
  model: function(){
    return this.store.find('milestone');
  },
  actions: {
    deleteMilestones: function(){
      this.controllerFor(this.routeName).set('confirmDelete', false);
      //this.store.delete('milestone')

      console.log(this.store.find('milestone'));
      this.store.all('milestone').destroyRecord();

    }
  }
});
