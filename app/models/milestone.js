import DS from 'ember-data';

export default DS.Model.extend({
  workout_id: DS.attr('number', {defaultValue: 0}),
  day_id: DS.attr('number', {defaultValue: 0}),
  week_id: DS.attr('number', {defaultValue: 0}),
  completed: DS.attr('date')
});
