import DS from 'ember-data';

export default DS.Model.extend({
  set: DS.attr('number'),
  weight: DS.attr('number'),
  reps: DS.attr('number'),
  workout: DS.attr('number'),
  day: DS.attr('number')
});