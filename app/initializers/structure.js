export default {
  name: 'structure',
  after: 'store',
  initialize: function(container, app) {
    app.deferReadiness();
    app.advanceReadiness();
  }
};