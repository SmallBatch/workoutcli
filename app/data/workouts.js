export default [
  // A
  {id: 1, sets: 5, reps: 5, exercise: 1, group: 1, dropSet: false, isComplete: false},
  {id: 2, sets: 4, reps: 8, exercise: 2, group: 1, dropSet: false, isComplete: false},
  {id: 3, sets: 5, reps: 5, exercise: 3, group: 1, dropSet: false, isComplete: false},
  {id: 4, sets: 3, reps: 8, exercise: 4, group: 1, dropSet: false, isComplete: false},
  {id: 5, sets: 4, reps: 8, exercise: 5, group: 1, dropSet: false, isComplete: false},
  // B
  {id: 6, sets: 5, reps: 5, exercise: 6, group: 2, dropSet: false, isComplete: false},
  {id: 7, sets: 4, reps: 8, exercise: 7, group: 2, dropSet: false, isComplete: false},
  {id: 8, sets: 3, reps: 8, exercise: 8, group: 2, dropSet: false, isComplete: false},
  {id: 9, sets: 4, reps: 12, exercise: 9, group: 2, dropSet: false, isComplete: false},
  // C
  {id: 10, sets: 3, reps: 12, exercise: 10, group: 3, dropSet: false, isComplete: false},
  {id: 11, sets: 3, reps: 12, exercise: 11, group: 3, dropSet: false, isComplete: false},
  {id: 12, sets: 3, reps: 12, exercise: 12, group: 3, dropSet: false, isComplete: false},
  {id: 13, sets: 3, reps: 12, exercise: 13, group: 3, dropSet: false, isComplete: false},
  {id: 14, sets: 3, reps: 15, exercise: 14, group: 3, dropSet: false, isComplete: false},
  {id: 15, sets: 3, reps: 15, exercise: 15, group: 3, dropSet: true, isComplete: false},
  // D
  {id: 16, sets: 3, reps: 12, exercise: 6, group: 4, dropSet: false, isComplete: false},
  {id: 17, sets: 2, reps: 12, exercise: 16, group: 4, dropSet: false, isComplete: false},
  {id: 18, sets: 2, reps: 12, exercise: 17, group: 4, dropSet: true, isComplete: false},
  {id: 19, sets: 2, reps: 12, exercise: 8, group: 4, dropSet: true, isComplete: false},
  {id: 20, sets: 2, reps: 12, exercise: 18, group: 4, dropSet: false, isComplete: false},
  {id: 21, sets: 3, reps: 20, exercise: 19, group: 4, dropSet: false, isComplete: false},
  // E
  {id: 22, sets: 4, reps: 20, exercise: 20, group: 5, dropSet: false, isComplete: false},

  {id: 23, sets: 3, reps: 15, exercise: 15, group: 5, dropSet: false, isComplete: false},
  {id: 24, sets: 3, reps: 15, exercise: 21, group: 5, dropSet: false, isComplete: false},
  {id: 25, sets: 3, reps: 15, exercise: 22, group: 5, dropSet: false, isComplete: false},

  {id: 26, sets: 3, reps: 15, exercise: 23, group: 5, dropSet: false, isComplete: false},
  {id: 27, sets: 4, reps: 15, exercise: 24, group: 5, dropSet: false, isComplete: false},
  {id: 28, sets: 4, reps: 15, exercise: 25, group: 5, dropSet: false, isComplete: false},
  // F
  {id: 29, sets: 1, reps: 10, exercise: 8, group: 6, dropSet: false, isComplete: false},
  {id: 30, sets: 1, reps: 10, exercise: 18, group: 6, dropSet: true, isComplete: false},
  {id: 31, sets: 2, reps: 10, exercise: 8, group: 6, dropSet: true, isComplete: false},
  {id: 32, sets: 2, reps: 15, exercise: 18, group: 6, dropSet: true, isComplete: false},
  // G
  {id: 33, sets: 2, reps: 10, exercise: 26, group: 7, dropSet: false, isComplete: false},
  {id: 34, sets: 1, reps: 10, exercise: 2, group: 7, dropSet: false, isComplete: false},
  {id: 35, sets: 1, reps: 10, exercise: 10, group: 7, dropSet: false, isComplete: false},
  {id: 36, sets: 2, reps: 10, exercise: 3, group: 7, dropSet: false, isComplete: false},
  {id: 37, sets: 1, reps: 10, exercise: 27, group: 7, dropSet: false, isComplete: false},
  {id: 38, sets: 1, reps: 10, exercise: 4, group: 7, dropSet: false, isComplete: false},
  // H
  {id: 39, sets: 2, reps: 10, exercise: 5, group: 8, dropSet: false, isComplete: false},
  {id: 40, sets: 2, reps: 10, exercise: 28, group: 8, dropSet: false, isComplete: false},
  {id: 41, sets: 2, reps: 10, exercise: 29, group: 8, dropSet: false, isComplete: false},
  {id: 42, sets: 1, reps: 10, exercise: 30, group: 8, dropSet: false, isComplete: false},
  {id: 43, sets: 1, reps: 10, exercise: 31, group: 8, dropSet: false, isComplete: false},
  {id: 44, sets: 1, reps: 10, exercise: 32, group: 8, dropSet: false, isComplete: false},
  // I
  {id: 45, sets: 3, reps: 15, exercise: 33, group: 9, dropSet: false, isComplete: false},
  {id: 46, sets: 3, reps: 15, exercise: 34, group: 9, dropSet: false, isComplete: false},

  {id: 47, sets: 3, reps: 15, exercise: 35, group: 9, dropSet: true, isComplete: false},

  {id: 48, sets: 3, reps: 15, exercise: 12, group: 9, dropSet: false, isComplete: false},
  {id: 49, sets: 3, reps: 15, exercise: 27, group: 9, dropSet: false, isComplete: false},

  {id: 50, sets: 3, reps: 15, exercise: 13, group: 9, dropSet: true, isComplete: false},

  {id: 51, sets: 4, reps: 15, exercise: 15, group: 9, dropSet: false, isComplete: false},
  {id: 52, sets: 4, reps: 15, exercise: 36, group: 9, dropSet: false, isComplete: false},
  // J
  {id: 53, sets: 3, reps: 20, exercise: 6, group: 10, dropSet: false, isComplete: false},
  {id: 54, sets: 3, reps: 20, exercise: 37, group: 10, dropSet: false, isComplete: false},
  {id: 55, sets: 3, reps: 30, exercise: 16, group: 10, dropSet: false, isComplete: false},
  {id: 56, sets: 3, reps: 30, exercise: 38, group: 10, dropSet: false, isComplete: false},
  {id: 57, sets: 1, reps: 50, exercise: 39, group: 10, dropSet: false, isComplete: false},
  {id: 58, sets: 5, reps: 20, exercise: 8, group: 10, dropSet: true, isComplete: false},
  {id: 59, sets: 3, reps: 30, exercise: 9, group: 10, dropSet: true, isComplete: false},
  // K
  {id: 60, sets: 3, reps: 20, exercise: 14, group: 11, dropSet: false, isComplete: false},
  {id: 61, sets: 3, reps: 20, exercise: 40, group: 11, dropSet: false, isComplete: false},
  {id: 62, sets: 3, reps: 20, exercise: 21, group: 11, dropSet: false, isComplete: false},
  {id: 63, sets: 2, reps: 50, exercise: 36, group: 11, dropSet: false, isComplete: false},
  {id: 64, sets: 3, reps: 20, exercise: 30, group: 11, dropSet: false, isComplete: false},
  {id: 65, sets: 3, reps: 20, exercise: 41, group: 11, dropSet: false, isComplete: false},
  // L
  {id: 66, sets: 3, reps: 12, exercise: 11, group: 12, dropSet: false, isComplete: false},
  {id: 67, sets: 3, reps: 12, exercise: 42, group: 12, dropSet: false, isComplete: false},
  {id: 68, sets: 3, reps: 12, exercise: 43, group: 12, dropSet: false, isComplete: false},
  {id: 69, sets: 3, reps: 12, exercise: 23, group: 12, dropSet: false, isComplete: false},

  {id: 70, sets: 3, reps: 15, exercise: 12, group: 12, dropSet: false, isComplete: false},
  {id: 71, sets: 3, reps: 15, exercise: 27, group: 12, dropSet: false, isComplete: false},
  {id: 72, sets: 3, reps: 15, exercise: 44, group: 12, dropSet: false, isComplete: false},

  {id: 73, sets: 3, reps: 15, exercise: 14, group: 12, dropSet: false, isComplete: false},
  {id: 74, sets: 3, reps: 15, exercise: 15, group: 12, dropSet: false, isComplete: false},
  {id: 75, sets: 3, reps: 15, exercise: 36, group: 12, dropSet: false, isComplete: false},

  {id: 76, sets: 3, reps: 20, exercise: 45, group: 12, dropSet: false, isComplete: false},
  {id: 77, sets: 3, reps: 20, exercise: 41, group: 12, dropSet: false, isComplete: false},
  // M
  {id: 78, sets: 1, reps: 20, exercise: 6, group: 13, dropSet: false, isComplete: false},
  {id: 79, sets: 3, reps: 20, exercise: 16, group: 13, dropSet: true, isComplete: false},
  {id: 80, sets: 4, reps: 20, exercise: 8, group: 13, dropSet: true, isComplete: false},
  {id: 81, sets: 3, reps: 40, exercise: 9, group: 13, dropSet: false, isComplete: false},
  // N
  {id: 82, sets: 3, reps: 12, exercise: 46, group: 14, dropSet: false, isComplete: false},
  {id: 83, sets: 3, reps: 12, exercise: 22, group: 14, dropSet: false, isComplete: false},
  {id: 84, sets: 3, reps: 12, exercise: 47, group: 14, dropSet: false, isComplete: false},
  {id: 85, sets: 3, reps: 12, exercise: 48, group: 14, dropSet: false, isComplete: false},

  {id: 86, sets: 3, reps: 50, exercise: 36, group: 14, dropSet: false, isComplete: false},
  {id: 87, sets: 3, reps: 30, exercise: 49, group: 14, dropSet: false, isComplete: false},

  {id: 88, sets: 3, reps: 20, exercise: 24, group: 14, dropSet: false, isComplete: false},
  {id: 89, sets: 3, reps: 20, exercise: 25, group: 14, dropSet: false, isComplete: false},

  {id: 90, sets: 2, reps: 20, exercise: 29, group: 14, dropSet: false, isComplete: false},
  {id: 91, sets: 2, reps: 20, exercise: 50, group: 14, dropSet: false, isComplete: false},
  // O
  {id: 92, sets: 4, reps: 15, exercise: 11, group: 15, dropSet: false, isComplete: false},
  {id: 93, sets: 4, reps: 15, exercise: 51, group: 15, dropSet: false, isComplete: false},
  {id: 94, sets: 4, reps: 15, exercise: 3, group: 15, dropSet: false, isComplete: false},
  {id: 95, sets: 3, reps: 15, exercise: 13, group: 15, dropSet: false, isComplete: false},
  {id: 96, sets: 4, reps: 20, exercise: 5, group: 15, dropSet: false, isComplete: false},
  // P
  {id: 97, sets: 4, reps: 25, exercise: 6, group: 16, dropSet: false, isComplete: false},
  {id: 98, sets: 3, reps: 15, exercise: 16, group: 16, dropSet: false, isComplete: false},
  {id: 99, sets: 4, reps: 15, exercise: 8, group: 16, dropSet: false, isComplete: false},
  {id: 100, sets: 3, reps: 10, exercise: 18, group: 16, dropSet: false, isComplete: false},
  {id: 101, sets: 3, reps: 30, exercise: 9, group: 16, dropSet: false, isComplete: false}
];