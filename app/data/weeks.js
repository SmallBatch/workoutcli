export default [
  {id: 1, days: [1,2,3,4], isComplete: false},
  {id: 2, days: [5,6,7,8], isComplete: false},
  {id: 3, days: [9,10,11,12], isComplete: false},
  {id: 4, days: [13,14,15,16], isComplete: false},
  {id: 5, days: [17,18,19,20], isComplete: false},
  {id: 6, days: [21,22,23,24], isComplete: false},
  {id: 7, days: [25,26,27,28], isComplete: false},
  {id: 8, days: [29,30,31,32], isComplete: false},
  {id: 9, days: [33,34,35,36], isComplete: false},
  {id: 10, days: [37,38,39,40], isComplete: false}
];