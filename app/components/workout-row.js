import Ember from 'ember';
import layout from '../templates/components/workout-row';

export default Ember.Component.extend({
  layout: layout,
  tagName: 'li',
  isEditing: false,
  store: null,
  day: null,
  actions: {
    edit: function(){
      this.set('isEditing', true);
    },
    cancelEdit: function(){
      this.set('isEditing', false);
    },
    saveWorkout: function(){

      var setRecords = this.get('workout.setRecords');
      setRecords.forEach(function(set){
        set.save();
      });
      this.set('workout.isComplete', true);
      this.sendAction('action', this.get('workout'));
      this.set('isEditing', false);
    }
  }
});
