import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    deleteMilestone: function(milestone) {
      milestone.destroyRecord();
    }
  }
});
