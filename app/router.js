import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource('exercises', function() {

  });
  this.resource('exercise', { path: 'exercise/:exercise_id' }, function() {});
  this.resource('weeks', function(){});
  this.resource('week', { path: 'week/:week_id' }, function() {

  });
  this.resource('days', function() {
    this.resource('day', { path: ':day_id' }, function() {

    });
  });
  this.resource('settings', function(){

  });
  this.resource('sets');
  this.resource('milestones');

});

export default Router;